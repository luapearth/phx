var webpack = require('webpack')
var path = require('path')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = require('./webpack.config')

module.exports.entry = {
  index: [path.join(__dirname, 'index.js')],
  header: [path.join(__dirname, 'src', 'components', 'Header', 'index.js')],
  card: [path.join(__dirname, 'src', 'components', 'Card', 'index.js')],
}

module.exports.output = {
  path: path.join(__dirname, 'dist'),
  filename: '[name].js',
  library: 'PhoenixRaw',
  libraryTarget: 'umd',
}

module.exports.externals = {
  react: {
    root: 'React',
    commonjs2: 'react',
    commonjs: 'react',
    amd: 'react',
  },
}

module.exports.plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
  })
)

module.exports.plugins.push(
  new ExtractTextPlugin({
    filename: 'css/[name].css',
  })
)

module.exports.plugins.push(new webpack.optimize.UglifyJsPlugin())
module.exports.plugins.push(new OptimizeCssAssetsPlugin())
