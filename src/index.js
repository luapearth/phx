import React, { Component } from 'react'
import { render } from 'react-dom'
import { Header, Card } from './components'
import './styles/app.scss'

class App extends Component {
  constructor(props) {
    super(props)
  }

  onChange({ value, name }) {
    console.log(value, name)
  }

  render() {
    return (
      <div>
        <Header title="Bonus Manager">[HEADER BUTTONS HERE]</Header>
        <div className="cs-flex">
          <Card title="New Message Plans">
            Test Promotion<br />24 October 2017<br />Bet Event Path = JY TEST EP C1<br />Channels = Telebet, Internet<br />Reward
            = 5000 Promo Money for 3 days, 1 time per account
          </Card>
          <Card title="JY Test 24Oct17 PromoGroup1" type="small" iconClass="phx-folder off" />
        </div>
      </div>
    )
  }
}

render(<App />, document.getElementById('root'))
