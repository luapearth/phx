import React from 'react'

const ListItem = props => {
  const { children, title, disabled, style, onClick, className } = props
  const addClass = disabled ? 'disabled' : null
  const onClickAction = !disabled ? onClick : null
  return (
    <li
      className={`button btn-box ${addClass} ${className}`}
      title={title}
      style={{ ...style }}
      onClick={onClickAction}
    >
      {children}
    </li>
  )
}

export default ListItem
