import React from 'react'
import PropTypes from 'prop-types'
import { Clock } from './'
import { hashHistory } from 'react-router'

const Header = ({ title, user, onClick, showLogOut, children, className, useDiffentList }) => {
  return (
    <header className={`width-wrapper header-container clearfix ${className}`}>
      <div className="header-content">
        <section className="brand clearfix fleft">
          <div
            className="logo fleft push-right"
            onClick={() => {
              hashHistory.replace('/')
            }}
          />
        </section>
        {title && (
          <nav className="navigation-container fleft">
            <ul className="nav">
              <li>
                <a className="active">{title}</a>
              </li>
            </ul>
          </nav>
        )}
        {children && (
          <section className="control-buttons fleft header-actions">
            {useDiffentList ? children : <ul className="list-reset button-group">{children}</ul>}
          </section>
        )}
        <section className="user-section fright">
          <div className="user-details">
            <div className="user-id">{user && `${user.username} [${user.id}]`}</div>
            {user && <Clock />}
          </div>
          {!showLogOut && (
            <div className="btn btn-outline btn-logout" onClick={onClick}>
              Logout
            </div>
          )}
        </section>
      </div>
    </header>
  )
}

export default Header

Header.propTypes = {
  user: PropTypes.object,
  title: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  useDiffentList: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.object, PropTypes.array]),
  showLogOut: PropTypes.bool.isRequired,
  className: PropTypes.string.isRequired,
}

Header.defaultProps = {
  useDiffentList: false,
  showLogOut: false,
  className: '',
  onClick: () => {
    console.log('logging out!')
  },
}
