import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import './Card.scss'

class Card extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.object, PropTypes.array]),
    type: PropTypes.oneOf(['small', 'large']).isRequired,
    iconClass: PropTypes.string.isRequired,
  }

  static defaultProps = {
    title: 'Message Plans',
    type: 'large',
    iconClass: 'phx-message-bulleted green',
  }

  constructor() {
    super()

    this.onClick = this.onClick.bind(this)
  }

  onClick() {
    this.props.onClick && this.props.onClick()
  }

  render() {
    const { children, title, type, iconClass } = this.props
    return (
      <div className={cx('cs-box__container', { 'large-box': type === 'large' })} onClick={this.onClick}>
        <div className="header">
          <div className="icon">
            <i className={iconClass} />
          </div>
          <h1>{title}</h1>
        </div>
        {type === 'large' && (
          <div className="content">
            <pre>{children}</pre>
          </div>
        )}
      </div>
    )
  }
}

export default Card
