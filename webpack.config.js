var path = require('path')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var autoprefixer = require('autoprefixer')

module.exports = {
  entry: {
    app: path.join(__dirname, 'src', 'index.js'),
    vendor: ['babel-polyfill', 'react', 'react-dom'],
  },

  output: {
    path: path.join(__dirname, 'public'),
    filename: 'js/[name].js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          publicPath: '../',
          use: [
            {
              loader: 'css-loader',
              options: { importLoaders: 1 },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer({ browsers: ['last 2 versions', 'last 3 iOS versions'] })],
              },
            },
            {
              loader: 'sass-loader',
            },
          ],
        }),
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          publicPath: '../',
          use: [
            {
              loader: 'css-loader',
              options: { importLoaders: 1 },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer({ browsers: ['last 2 versions', 'last 3 iOS versions'] })],
              },
            },
          ],
        }),
      },
      {
        test: [/\.(svg|jpe?g|gif|png|ico)$/],
        use: [
          {
            loader: 'file-loader?name=images/[name].[ext]',
          },
          {
            loader: 'image-webpack-loader',
          },
        ],
      },
      {
        test: /\.((ttf|eot|woff2?)(\?v=[0-9]\.[0-9]\.[0-9]))|(ttf|eot|woff2?)$/,
        use: 'file-loader?name=[name].[ext]&outputPath=fonts/',
      },
    ],
  },
  plugins: [],
}
